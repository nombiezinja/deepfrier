package util

import (
	"log"
	"strconv"
)

func CheckErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func ReturnInt(str string) int64 {
	var result int64
	if result, err := strconv.ParseInt(str, 10, 64); err == nil {
		return result
	}
	return result
}

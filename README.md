# A practice web app for learning Go

Purpose:
1) Consolidate writing db transactions
2) Set up RabbitMQ, create db transactions from front end through message queue

Set up a table to run the app with on the psql commandline:

```sql
CREATE TABLE orders (
  id SERIAL PRIMARY KEY,
  action VARCHAR(256),
  price BIGINT,
  amount BIGINT,
  token VARCHAR(256)
);
```

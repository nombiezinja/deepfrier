package main

import (
	"bitbucket.com/nombiezinja/deepfrier/cmd"
	"bitbucket.com/nombiezinja/deepfrier/database"
	"bitbucket.com/nombiezinja/deepfrier/server"
)

func main() {
	cmd.Setenv()
	database.InitDB()
	server.Serve()
}

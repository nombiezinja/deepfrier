package data

type Order struct {
	ID     int
	Token  string
	Action string
	Amount int64
	Price  int64
}

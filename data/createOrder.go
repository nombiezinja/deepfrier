package data

import (
	"bitbucket.com/nombiezinja/deepfrier/database"
)

func CreateOrder(order Order) (int, int64, error) {

	insertstr := "INSERT INTO orders(token, action, price, amount) VALUES ($1,$2,$3,$4) RETURNING id"
	stmt, err := database.DB.Prepare(insertstr)
	res, err := stmt.Exec(order.Token, order.Action, order.Price, order.Amount)
	if err != nil {
		return 0, 0, err
	}

	orderId := 0
	err = database.DB.QueryRow(insertstr, order.Token, order.Action, order.Price, order.Amount).Scan(&orderId)
	if err != nil {
		return 0, 0, err
	}

	rowCnt, err := res.RowsAffected()
	if err != nil {
		return 0, 0, err
	}

	return orderId, rowCnt, nil
}

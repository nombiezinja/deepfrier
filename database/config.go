package database

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"

	"bitbucket.com/nombiezinja/deepfrier/util"
	_ "github.com/lib/pq"
)

var DB *sql.DB

func portnum() int {
	port, err := strconv.Atoi(os.Getenv("port"))
	util.CheckErr(err)
	return port
}

func InitDB() {
	var err error
	dbstring := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s",
		os.Getenv("host"), portnum(), os.Getenv("username"), os.Getenv("password"), os.Getenv("dbname"))

	DB, err = sql.Open("postgres", dbstring)
	util.CheckErr(err)

	err = DB.Ping()
	util.CheckErr(err)

	fmt.Println("Db connection established")

}

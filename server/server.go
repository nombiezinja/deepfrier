package server

import (
	"log"
	"net/http"
)

type pageVars struct {
	DateTime string
}

// Serve starts server and serves routes
func Serve() {
	http.HandleFunc("/", Index)
	http.HandleFunc("/write", Write)
	log.Fatal(http.ListenAndServe(":3000", nil))
}

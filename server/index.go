package server

import (
	"html/template"
	"log"
	"net/http"
	"time"
)

// Index handles the route GET '/'
func Index(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	IndexVars := pageVars{
		DateTime: now.Format("Mon Jan _2 15:04:05 2006"),
	}

	t, err := template.ParseFiles(("template/index.html"))
	if err != nil {
		log.Print("Template execution error: ", err)
	}

	err = t.Execute(w, IndexVars)
	if err != nil {
		log.Print("Template execution error:", err)
	}

}

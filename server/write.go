package server

import (
	"fmt"
	"net/http"

	"bitbucket.com/nombiezinja/deepfrier/data"
	"bitbucket.com/nombiezinja/deepfrier/util"
)

// Write handles GET and POST requests to '/write'
func Write(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		fmt.Fprintf(w, "This page is useless hurhur")
	} else {
		order := data.Order{}
		err := r.ParseForm()

		if err != nil {
			fmt.Println(fmt.Errorf("Error: %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		fmt.Println("token:", r.Form["token"])
		fmt.Println("action:", r.Form["action"])
		fmt.Println("amount:", r.Form["amount"])
		fmt.Println("price:", r.Form["price"])

		order.Token = r.Form.Get("token")
		order.Action = r.Form.Get("action")
		order.Price = util.ReturnInt(r.Form.Get("price"))
		order.Amount = util.ReturnInt(r.Form.Get("amount"))

		orderId, rowCnt, err := data.CreateOrder(order)
		util.CheckErr(err)

		fmt.Fprintf(w, "Order entry id:  %d, number of orders saved: %d\n", orderId, rowCnt)
	}

}
